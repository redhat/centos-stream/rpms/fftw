#!/bin/sh -eux

# Create the test program source file
cat << 'EOF' > fftw_test.c
#include <stdio.h>
#include <math.h>
#include <fftw3.h>

int main() {
    int N = 4;

    fftw_complex in[N], out[N], back[N];
    fftw_plan forward_plan, backward_plan;

    // Initialize input array with known values
    for (int i = 0; i < N; i++) {
        in[i][0] = (double)(i + 1); // Real part: 1.0, 2.0, 3.0, 4.0
        in[i][1] = 0.0;             // Imaginary part: 0.0
    }

    forward_plan = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    backward_plan = fftw_plan_dft_1d(N, out, back, FFTW_BACKWARD, FFTW_ESTIMATE);

    fftw_execute(forward_plan);

    printf("Forward FFT Output (Frequency Domain):\n");
    for (int i = 0; i < N; i++) {
        printf("Index %d: %f + %fi\n", i, out[i][0], out[i][1]);
    }

    fftw_execute(backward_plan);

    // Normalize the output of the inverse FFT
    for (int i = 0; i < N; i++) {
        back[i][0] /= N;
        back[i][1] /= N;
    }

    printf("\nOriginal Input vs. Recovered Data:\n");
    for (int i = 0; i < N; i++) {
        printf("Index %d:\n", i);
        printf("  Original:  %f + %fi\n", in[i][0], in[i][1]);
        printf("  Recovered: %f + %fi\n", back[i][0], back[i][1]);
    }

    fftw_destroy_plan(forward_plan);
    fftw_destroy_plan(backward_plan);

    return 0;
}
EOF

# Compile the test program
gcc -o fftw_test fftw_test.c -lfftw3 -lm

# Run the test program and capture its output
OUTPUT=$(./fftw_test)

# Expected output patterns (using grep-compatible regular expressions)
EXPECTED_PATTERNS=(
    "Forward FFT Output \(Frequency Domain\):"
    "Index 0: 10\.000000 \+ 0\.000000i"
    "Index 1: -2\.000000 \+ 2\.000000i"
    "Index 2: -2\.000000 \+ 0\.000000i"
    "Index 3: -2\.000000 \+ -2\.000000i"
    "Original Input vs\. Recovered Data:"
    "Index 0:"
    "  Original:  1\.000000 \+ 0\.000000i"
    "  Recovered: 1\.000000 \+ 0\.000000i"
    "Index 1:"
    "  Original:  2\.000000 \+ 0\.000000i"
    "  Recovered: 2\.000000 \+ 0\.000000i"
    "Index 2:"
    "  Original:  3\.000000 \+ 0\.000000i"
    "  Recovered: 3\.000000 \+ 0\.000000i"
    "Index 3:"
    "  Original:  4\.000000 \+ 0\.000000i"
    "  Recovered: 4\.000000 \+ 0\.000000i"
)

# Verify the output
ALL_MATCH=1
for PATTERN in "${EXPECTED_PATTERNS[@]}"; do
    if ! echo "$OUTPUT" | grep -qE "$PATTERN"; then
        echo "Pattern not found: $PATTERN"
        ALL_MATCH=0
    fi
done

if [ $ALL_MATCH -eq 1 ]; then
    echo "FFTW test passed."
    exit 0
else
    echo "FFTW test failed."
    echo "Program output:"
    echo "$OUTPUT"
    exit 1
fi

